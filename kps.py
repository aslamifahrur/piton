import OPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)
GPIO.setup(24, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)

try:
 print("Press CTRL+C to Exit")
 while True:
  print("HIGH")
  GPIO.output(24, True)
  GPIO.output(23, False)
  time.sleep(5)
  
  print("LOW")
  GPIO.output(12, False)
  GPIO.output(12, False)
  time.sleep(5)

except KeyboardInterrupt:
 GPIO.output(24, False)
 GPIO.output(23, False)
 print("Bye")

finally:
 GPIO.cleanup()